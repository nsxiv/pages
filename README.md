# nsxiv-pages

This repo is used for hosting the [nsxiv website](https://nsxiv.codeberg.page).

## Build

The website is generated from the README and the manpage, thus it is required
that nsxiv repo is somewhere on disk. The env var `nsxiv_repo` needs to be set
so that it points to the nsxiv repo. By default, `${nsxiv_repo}` is set to `..`.

```console
$ nsxiv_repo="/path/to/nsxiv" ./build.sh
```
