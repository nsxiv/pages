# Insert man page link *before* the main heading
/<h2 id="neo-or-new-or-not-simple-or-small-or-suckless-x-image-viewer"><strong>Neo (or New or Not) Simple (or Small or Suckless) X Image Viewer<\/strong><\/h2>/i\
<p><a href="https://nsxiv.codeberg.page/man/">View Man Page</a></p>\
<p><a href="https://nsxiv.codeberg.page/changelog/">View Changelog</a></p>
